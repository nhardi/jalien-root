# Find module for libwebsockets.
# Variables:
#  - LWS_FOUND: TRUE if found
#  - LWS_LIBPATH: library path
#  - LWS_INCLUDE_DIR: include dir
include(FindPackageHandleStandardArgs)
find_library(LWS_LIBPATH "websockets"
             PATH_SUFFIXES "lib"
             HINTS "${LWS}")
find_path(LWS_INCLUDE_DIR "libwebsockets.h"
          PATH_SUFFIXES "include"
          HINTS "${LWS}")
get_filename_component(LWS_LIBPATH ${LWS_LIBPATH} DIRECTORY)
find_package_handle_standard_args(LWS DEFAULT_MSG
                                  LWS_LIBPATH LWS_INCLUDE_DIR)
include_directories(${LWS_INCLUDE_DIR})
link_directories(${LWS_LIBPATH})
