/*
 * TJAlienJobStatus.h
 *
 *  Created on: Sep 4, 2014
 *      Author: Tatianka Tothova
 */

#ifndef ROOT_TJAlienJobStatus
#define ROOT_TJAlienJobStatus

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienJobStatus                                                      //
//                                                                      //
// JAliEn implementation of TGridJobStatus.                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TAliceJobStatus.h"
#include "TMap.h"

class TJAlienJob;
class TJAlienMasterJob;

class TJAlienJobStatus : public TAliceJobStatus {

friend class TJAlienJob;
friend class TJAlienMasterJob;

private:
    TMap fStatus;     // Contains the status information of the job.
                      // In the Alien implementation this is a string, string map.
    TString fJdlTag;  // JdlTag

public:
    TJAlienJobStatus() {};
    TJAlienJobStatus(TMap *status);
    virtual ~TJAlienJobStatus();

    const char *GetJdlKey(const char *key);
    const char *GetKey(const char *key);

    virtual TGridJobStatus::EGridJobStatus GetStatus() const;
    virtual void Print(Option_t *) const;
    virtual void PrintJob(Bool_t full) const;
    
    Bool_t IsFolder() const { return kTRUE;}
    void Browse(TBrowser *b);

    ClassDef(TJAlienJobStatus,1)  // JAliEn implementation of TGridJobStatus
};

#endif /* ROOT_TJAlienJobStatus */
