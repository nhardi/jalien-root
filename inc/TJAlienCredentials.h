// Author: Nikola Hardi   3/6/2019
#ifndef ROOT_TJAlienCredentials
#define ROOT_TJAlienCredentials
#include <string>
#include <map>
#include <termios.h>
#include "TObject.h"

using std::string;
using std::map;

enum CredentialsKind { cJBOX_TOKEN = 0,
                       cFULL_GRID_CERT,
                       cJOB_TOKEN,
                       cOTHER_TOKEN,
};


class TJAlienCredentialsObject {
 public:
    string certpath;
    string keypath;
    string password;
    CredentialsKind kind;
    bool autoremove;

    TJAlienCredentialsObject() {}
    TJAlienCredentialsObject(string certpath,
                             string keypath,
                             CredentialsKind kind = cOTHER_TOKEN,
                             bool autoremove = false)
    {
        this->certpath = certpath;
        this->keypath = keypath;
        this->kind = kind;
        this->autoremove = autoremove;
    };

    void wipe() {
        if(autoremove) {
            if(gDebug) printf("removing safe files: %s %s\n", certpath.c_str(), keypath.c_str());
            remove(certpath.c_str());
            remove(keypath.c_str());
        }
    }

    bool exists();
    const string getKey();
    const string getCertificate();
    const string getPassword();
    void readPassword();
};

class TJAlienCredentials : public TObject {
public:
  TJAlienCredentials();
  ~TJAlienCredentials();

  static string getTmpDir();
  static string getHomeDir();
  void loadCredentials();

  bool has(CredentialsKind kind);
  TJAlienCredentialsObject get(CredentialsKind kind);
  void removeCredentials(CredentialsKind kind);
  short count();

  static const char *ENV_JOBTOKEN_KEY;
  static const char *ENV_JOBTOKEN_CERT;
  static const char *TMP_JOBTOKEN_KEY_FNAME_PREFIX;
  static const char *TMP_JOBTOKEN_CERT_FNAME_PREFIX;

private:
  void loadTokenCertificate();
  void loadFullGridCertificate();
  void loadJobTokenCertificate();
  string getUsercertPath();
  string getUserkeyPath();
  string getTokencertPath();
  string getTokenkeyPath();

  string getSafeFilename(const string& prefix);
  void writeSafeFile(const string& filepath, const string& content);

  string tmpdir;
  string homedir;
  map<CredentialsKind, TJAlienCredentialsObject> found_credentials;

  ClassDef(TJAlienCredentials, 0)
};
#endif
