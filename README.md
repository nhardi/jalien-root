JAliEn implementation of the TGrid interface
============================================

This is the JAliEn client version for ROOT. It compiles as a ROOT plugin and
handles `alien:` Grid connections and `alien://` files.


Installation
------------

The installation procedure is based on aliBuild and uses ROOT 6. Please refer to
[this guide](https://alice-doc.github.io/alice-analysis-tutorial/building/) for
base aliBuild instructions.

Before you start, you must get the build recipes from the experimental
repository:

```bash
rm -rf alidist  # make a backup first if you wish
git clone https://github.com/alisw/alidist
```

Since you are going to develop JAliEn-ROOT, have it ready as "development
package" as seen by aliBuild:

```bash
aliBuild init JAliEn-ROOT@master
```

The project will be cloned as a Git repository.

Now follow the steps from the guide above, but build the JAliEn plugin instead
of AliRoot/AliPhysics:

```bash
aliBuild build JAliEn-ROOT --defaults user-next-root6
```

To use the plugin there is no need to load the environment permanently, it is
sufficient to load ROOT within the proper environment:

```bash
alienv setenv JAliEn-ROOT/latest -c root -l -b
```
