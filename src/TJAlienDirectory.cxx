// @(#)root/alien:$Id$
// Author: Jan Fiete Grosse-Oetringhaus   28/9/2004

/*************************************************************************
 * Copyright (C) 1995-2004, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienDirectory                                                      //
//                                                                      //
// Class which creates Directory files for the AliEn middleware         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TJAlienDirectory.h"
#include "TJAlien.h"
#include "TGridResult.h"
#include "TSystemFile.h"
#include "TJAlienFile.h"
#include "TJAlienSystem.h"
#include "TFile.h"
#include "TObjString.h"


ClassImp(TJAlienDirectoryEntry)

//______________________________________________________________________________
void TJAlienDirectoryEntry::Browse(TBrowser* b)
{
    // Browse an Alien directory.

    if (b) {
        TString jalienname = "alien://";
        jalienname += fLfn;
        TObject *bobj;
        if (!(bobj = fBrowserObjects.FindObject(jalienname.Data()))) {
            TFile *newfile = TFile::Open(jalienname.Data());
            b->Add(newfile);
            fBrowserObjects.Add(new TObjString(jalienname.Data()), (TObject*) newfile);
        }
    }
}


ClassImp(TJAlienDirectory)

//______________________________________________________________________________
TJAlienDirectory::TJAlienDirectory(const char *ldn, const char *name)
{
    // Constructor.

    if (!gGrid->Cd(ldn)) {
        MakeZombie();
        return;
    }

    if (!name) {
        SetName(gSystem->BaseName(ldn));
    } else {
        SetName(name);
    }

    SetTitle(ldn);
};

//______________________________________________________________________________
void TJAlienDirectory::Fill()
{
    // Fill directory entry list.

    if (!gGrid->Cd(GetTitle())) {
        MakeZombie();
        return;
    }

    fEntries.Clear();
    TGridResult *dirlist = gGrid->Ls(GetTitle(), "-la");
    if (dirlist) {
        dirlist->Sort();
        Int_t i = 0;
        while (dirlist->GetFileName(i)) {
            if (!strcmp(".",dirlist->GetFileName(i))) {
                i++;
                continue;
            }
            if (!strcmp("..",dirlist->GetFileName(i))) {
                i++;
                continue;
            }

            if (dirlist->GetKey(i,"permissions")[0] == 'd') {
                fEntries.Add(new TJAlienDirectory(dirlist->GetFileNamePath(i)));
            } else {
                fEntries.Add(new TJAlienDirectoryEntry(dirlist->GetFileNamePath(i), dirlist->GetFileName(i)));
            }
            i++;
        }
        delete dirlist;
    }
}

//______________________________________________________________________________
void TJAlienDirectory::Browse(TBrowser *b)
{
    // Browser interface to ob status.

    if (b) {
        Fill();
        TIter next(&fEntries);
        TObject *obj = 0;
        TObject *bobj = 0;
        while ((obj = next())) {
            if (!(bobj = fBrowserObjects.FindObject(obj->GetName()))) {
                b->Add(obj, obj->GetName());
                fBrowserObjects.Add(new TObjString(obj->GetName()), (TObject*) obj);
            } else {
                b->Add(bobj, bobj->GetName());
            }
        }
    }
}

//______________________________________________________________________________
TJAlienDirectory::~TJAlienDirectory()
{
    // Destructor.

    fEntries.Clear();
}
