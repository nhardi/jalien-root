#include "TJAlienCredentials.h"
#include <sstream>
#include <cstdlib>
#include <unistd.h>
#include <fstream>
#include <cstdlib>
#include <iostream>
#include <fcntl.h>

using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::endl;
using std::getenv;

const char* TJAlienCredentials::ENV_JOBTOKEN_KEY = "JALIEN_TOKEN_KEY";
const char* TJAlienCredentials::ENV_JOBTOKEN_CERT = "JALIEN_TOKEN_CERT";

const char* TJAlienCredentials::TMP_JOBTOKEN_KEY_FNAME_PREFIX = "tmpjobtokenkey_";
const char* TJAlienCredentials::TMP_JOBTOKEN_CERT_FNAME_PREFIX = "tmpjobtokencert_";

bool fileExists(const string &filename)
{
    bool fileExists = false;
    FILE *f = fopen(filename.c_str(), "r");

    if (f != NULL ) {
        fclose(f);
        fileExists = true;
    } else {
        fileExists = false;
    }

    return fileExists;
}


bool TJAlienCredentialsObject::exists() {
    return fileExists(certpath) && fileExists(keypath);
}

void TJAlienCredentials::writeSafeFile(const string& filename, const string& content) {
    if(gDebug) printf("writing safe file %s\n", filename.c_str());
    int fd = open(filename.c_str(), O_RDWR | O_CREAT, 0600);
    write(fd, content.c_str(), content.length());
    close(fd);
}

string TJAlienCredentials::getSafeFilename(const string& prefix) {
    string filename = TJAlienCredentials::getTmpDir() + "/" + prefix;

    const char *JOB_ID = getenv("ALIEN_PROC_ID");

    if(JOB_ID != NULL) {
        filename += JOB_ID;
    } else {
        pid_t pid = getpid();
        unsigned int rnd = random() % 100 + 1;

        do {
            filename += std::to_string(pid) + "_" + std::to_string(rnd);
        } while(fileExists(filename));
    }


    return filename;
}

string TJAlienCredentials::getTmpDir() {
	string tmpdir;

	if (getenv("TMPDIR") != NULL)
		tmpdir = getenv("TMPDIR");
	else if (getenv("TMP") != NULL)
		tmpdir = getenv("TMP");
	else if (getenv("TEMP") != NULL)
		tmpdir = getenv("TEMP");
	else
		tmpdir = P_tmpdir;

	return tmpdir;
}

string TJAlienCredentials::getHomeDir() {
	string homedir;

	if (getenv("HOME") != NULL)
		homedir = getenv("HOME");
	else
		homedir = "~";

	return homedir;
}

string TJAlienCredentials::getTokencertPath()
{
	std::stringstream tokencert_s;
	tokencert_s << tmpdir << "/tokencert_" << getuid() << ".pem";
	std::string tokencert = tokencert_s.str();
	std::string tokencertpath = std::getenv("JALIEN_TOKEN_CERT") ? : tokencert;

	return tokencertpath;
}

string TJAlienCredentials::getTokenkeyPath()
{
	std::stringstream tokenkey_s;
	tokenkey_s << tmpdir << "/tokenkey_" << getuid() << ".pem";
	std::string tokenkey = tokenkey_s.str();
	std::string tokenkeypath = std::getenv("JALIEN_TOKEN_KEY") ? : tokenkey;

	return tokenkeypath;
}

string TJAlienCredentials::getUsercertPath()
{
    std::string usercert = homedir + "/.globus/usercert.pem";
    std::string usercertpath = std::getenv("X509_USER_CERT") ? : usercert;
    return usercertpath;
}

string TJAlienCredentials::getUserkeyPath()
{
    std::string userkey = homedir + "/.globus/userkey.pem";
    std::string userkeypath = std::getenv("X509_USER_KEY") ? : userkey;
    return userkeypath;
}

TJAlienCredentials::TJAlienCredentials() {
    tmpdir = getTmpDir();
    homedir = getHomeDir();
}

void TJAlienCredentials::loadCredentials() {
  removeCredentials(cJOB_TOKEN);
	found_credentials.clear();
	loadTokenCertificate();
	loadFullGridCertificate();
	loadJobTokenCertificate();
}

void TJAlienCredentials::loadTokenCertificate() {
    TJAlienCredentialsObject token_credentials(getTokencertPath(), getTokenkeyPath(), cJBOX_TOKEN);

    if(token_credentials.exists()) {
        found_credentials[cJBOX_TOKEN] = token_credentials;
    }
}

void TJAlienCredentials::loadFullGridCertificate() {
    TJAlienCredentialsObject grid_certificate(getUsercertPath(), getUserkeyPath(), cFULL_GRID_CERT);

    if (grid_certificate.exists()) {
        found_credentials[cFULL_GRID_CERT] = grid_certificate;
    }
}

void TJAlienCredentials::loadJobTokenCertificate() {
    const char *env_cert = getenv(ENV_JOBTOKEN_CERT);
    const char *env_key = getenv(ENV_JOBTOKEN_KEY);

    // if it doesn't have both environment variables
    if(!env_cert || !env_key) {
        return;
    }

    // environment variables contain valid filepaths instead of the actual token
    if(fileExists(env_cert) && fileExists(env_key)) {
        found_credentials[cJOB_TOKEN] = TJAlienCredentialsObject(env_cert, env_key, cJOB_TOKEN);
    } else {
        const string& tmpcertpath = getSafeFilename(TMP_JOBTOKEN_CERT_FNAME_PREFIX);
        writeSafeFile(tmpcertpath, env_cert);

        const string& tmpkeypath = getSafeFilename(TMP_JOBTOKEN_KEY_FNAME_PREFIX);
        writeSafeFile(tmpkeypath, env_key);

        found_credentials[cJOB_TOKEN] = TJAlienCredentialsObject(tmpcertpath, tmpkeypath, cJOB_TOKEN, true);
    }
}

bool TJAlienCredentials::has(CredentialsKind kind) {
    return found_credentials.count(kind) == 1;
}

TJAlienCredentialsObject TJAlienCredentials::get(CredentialsKind kind) {
    if(this->has(kind)) {
        return found_credentials[kind];
    } else {
      return TJAlienCredentialsObject();
    }
}

void TJAlienCredentials::removeCredentials(CredentialsKind kind) {
	if (this->has(kind)) {
      if(kind == cJOB_TOKEN)
          get(kind).wipe();

		found_credentials.erase(kind);
	}
}

short TJAlienCredentials::count() {
	return found_credentials.size();
}

string readFile(const char* filename) {
    string line;
    stringstream contents;
    ifstream f(filename);

    if(f.is_open()) {
        while(getline(f, line)) {
            contents << line << endl;
        }
    }
    return contents.str();
}

const string TJAlienCredentialsObject::getKey() {
    return readFile(keypath.c_str());
}

const string TJAlienCredentialsObject::getCertificate() {
    return readFile(certpath.c_str());
}

void TJAlienCredentialsObject::readPassword() {
	if (this->kind == cFULL_GRID_CERT && this->getKey().find("ENCRYPTED") != std::string::npos)
	{
		printf("[Grid certificate password: ]");
		struct termios termold, termnew;
		tcgetattr(fileno(stdin), &termold);
		termnew = termold;
		termnew.c_lflag &= ~ECHO;
		termnew.c_lflag |= ECHONL;
		tcsetattr(fileno(stdin), TCSANOW, &termnew);
		
		char password[64];
		if (!fgets(password, sizeof(password), stdin))
			printf("\nFailed to get password input\n");
		
		tcsetattr(0, TCSANOW, &termold);
		
		password[strlen(password) - 1] = 0;
		this->password = std::string(password);
		}
}

const string TJAlienCredentialsObject::getPassword() {
	if (this->password.empty())
		readPassword();

    return this->password;
}

TJAlienCredentials::~TJAlienCredentials() {
    removeCredentials(cJOB_TOKEN);
}
